
#include <imgui.h>
#include "imgui_impl_glfw_gl3.h"
#include <stdio.h>
#include <GL/gl3w.h>
#include <GLFW/glfw3.h>
#include "nanovg.h"
#include "nanovg_gl.h"

static void error_callback(int error, const char* description)
{
    fprintf(stderr, "Error %d: %s\n", error, description);
}

int main(int, char**)
{
    // Setup window

    if (!glfwInit())
        return 1;

    glfwSetErrorCallback(error_callback);
#ifndef _WIN32 // don't require this on win32, and works with more cards
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#endif
    glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, 1);
	glfwWindowHint(GLFW_SAMPLES, 4);

    GLFWwindow* window = glfwCreateWindow(1280, 720, "ImGui OpenGL3 example", NULL, NULL);
    glfwMakeContextCurrent(window);
    glfwSwapInterval(1); // Enable vsync

    gl3wInit();

    struct NVGcontext* vg = nvgCreateGL3(NVG_ANTIALIAS | NVG_STENCIL_STROKES | NVG_DEBUG);
    if (vg == NULL) {
        printf("Could not init nanovg.\n");
        return -1;
    }

    // Setup ImGui binding
    ImGui_ImplGlfwGL3_Init(window, true);

    // Setup style
    ImGui::StyleColorsClassic();
    //ImGui::StyleColorsDark();

    float angle0=0.0;
    float angle1=NVG_PI;
    int radius=300;
    float width=10.0f;
    bool CCW=true;
    ImVec4 arc_color = ImVec4(1.0, 1.00f, 0.00f, 1.00f);
    ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);

    // Main loop
    while (!glfwWindowShouldClose(window))
    {
        // You can read the io.WantCaptureMouse, io.WantCaptureKeyboard flags to tell if dear imgui wants to use your inputs.
        // - When io.WantCaptureMouse is true, do not dispatch mouse input data to your main application.
        // - When io.WantCaptureKeyboard is true, do not dispatch keyboard input data to your main application.
        // Generally you may always pass all inputs to dear imgui, and hide them from your application based on those two flags.
        glfwPollEvents();
        ImGui_ImplGlfwGL3_NewFrame();

        // 1. Show a simple window.
        // Tip: if we don't call ImGui::Begin()/ImGui::End() the widgets automatically appears in a window called "Debug".
        {
            ImGui::SliderFloat("Angle Start", &angle0, 0.0f, 2.0*NVG_PI);
            ImGui::SliderFloat("Angle End", &angle1, 0.0f, 2.0*NVG_PI);
            ImGui::SliderInt("Arc Radius", &radius, 0, 2000);
            ImGui::SliderFloat("Stroke Width", &width, 0.0f, 100.0);
            ImGui::Checkbox("CCW?", &CCW);            
            ImGui::ColorEdit3("arc color", (float*)&arc_color);
            ImGui::ColorEdit3("clear color", (float*)&clear_color);
            ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
        }

        // Rendering
        int window_w, window_h;
        int display_w, display_h;
        glfwGetWindowSize(window, &window_w, &window_h);
        glfwGetFramebufferSize(window, &display_w, &display_h);
        float pxRatio = (float)display_w / (float)window_w;

        glViewport(0, 0, display_w, display_h);
        glClearColor(clear_color.x, clear_color.y, clear_color.z, clear_color.w);
        glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT|GL_STENCIL_BUFFER_BIT);

        {
            nvgBeginFrame(vg, display_w, display_h, pxRatio);

            nvgLineCap(vg, NVG_ROUND );
            nvgStrokeColor(vg, nvgRGBAf(arc_color.x,arc_color.y,arc_color.z,arc_color.w));
            nvgStrokeWidth(vg, width);

            nvgBeginPath(vg);
            nvgArc(vg, 
                display_w/2,display_h/2, 
                radius, 
                angle0, angle1, 
                CCW ? NVG_CCW : NVG_CW);
            nvgStroke(vg);

            nvgEndFrame(vg);
        }
        // Cleanup state here?

        ImGui::Render();
        glfwSwapBuffers(window);
    }

    // Cleanup
    ImGui_ImplGlfwGL3_Shutdown();
    glfwTerminate();

    return 0;
}
